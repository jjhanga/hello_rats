# Hello_RATS

git
===========


버전관리 프로그램(문서나 코드 등의 변화 과정을 모두 기록함) 

즉, 버전관리를위해 특별히 백업본을 만들어 두지 않아도 되는 프로그램.

일반적으로 알고있는 github은 이 git을 사용한 서비스 저장소임.
github를 사용하지 않더라도 git은 사용가능함.


## git 툴

커맨드라인 | GUI
---|---
git bash(기본제공) | source tree
 | github 데스크탑
 | 등등....
 
 비주얼 스튜디오 등의 툴은 내부에서 git툴을 제공 하는 경우도 있음.
 


git저장소 | 특징
---|---
github | 가장 유명함(사람이 많은 비공개 프로젝트는 유료)
gitlab | 인원수에 관계없이 무제한으로 무료 비공개 프로젝트가 가능하다.
bitbucket | 5명이하 프로젝트는 무료 비공개 프로젝트 가능



-------------

## git 사용방법
		
		먼저 git 서비스 내에서 repository를 만들어야 함.
		repository를 만들고 해당 프로젝트의 맴버로 사람들을 추가하면 해당 사람들끼리 프로젝트를 공유할 수 있음.
		bitbucket 에서 아이디를 만들고 repository 생성후 user and group access에서 
		
		kof5656@naver.com 
		apollyon4@naver.com 
		
		을 맴버로 추가시켜 권한을 admin으로 설정하고,
		이후 과제는 해당 repository에 추가로 업로드하면 됨.
		repository의 이름은 본인의 이름으로 해주세요.


-------------

## 가장 기초적인 사용법 - 커맨드라인으로 사용할 경우

		내가 프로젝트를 진행할 폴더에서 git bash를 실행 시킴.
		그 후 커맨드라인에서

		git config --global user.name "이름"
		git config --global user.email "깃서비스 메일주소" 
		// 매번 물어보는 귀찮음을 피하기 위해 설정.
		// 설정하지 않으면 push를 할 때마다 매번 로그인을 해야할 수도 있음.
		// --global은 내가 진행하는 모든 프로젝트에 적용되는 사항으로 프로젝트마다 다르게 하고싶다면 --global없이 하면됨.

		mkdir ~/MyProject   // 로컬 디렉토리 만들고
		cd ~/myproject      // 디렉토리로 들어가서
		git init            // 깃 명령어를 사용할 수 있는 디렉토리로 만든다.
			해당 명령어를 사용하면 디렉토리 내에 .git 이라는 숨김폴더가 만들어짐
			그 폴더내부에 버전정보가 저장됨.

		git status          // 현재 상태를 훑어보고(생략가능)
		git add 화일명.확장자  // 깃 주목 리스트에 화일을 추가하고 or
		git add .           // 이 명령은 현재 디렉토리의 모든 화일을 추가할 수 있다.
		git commit -m “코멘트” // 커밋해서 스냅샷을 찍는다.

		git remote add origin https://github.com/xxxxxxxxx/xxxxxx.git // 로컬과 원격 저장소를 연결한다.(한번 하면 그 후로는 할 필요 없음)
		git remote -v // 연결상태를 확인한다.
		git push origin master // 저장소로 푸시한다.

		그외에 에러가 날 수 있는 경우

		나와 다른사람이 같은 파일을 수정한 후 push를 하면 에러가 발생함.(파일 동시수정 방지)


명령어 | 기능
---|---
add | git에 넣기 전 마지막 확인작업(얼마든지 취소가능)
commit | 직접 git에 해당 버전을 업데이트 함(로컬로 저장함. 취소안됨. commit을 하기전에는 실수한 것이 없나 따져봐야함.)
push | commit된 내용을 바탕으로 git서비스에 업로드함(서버에 저장)
